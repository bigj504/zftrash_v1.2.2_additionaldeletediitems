ZFTrash = CreateFrame("Frame")
ZFTrash.active = false

ZFTrash:SetScript("OnUpdate", function()
    if (ZFTrash.tick or 5) > GetTime() then return else ZFTrash.tick = GetTime() + 5 end

    if ZFTrash.active then
        for bag = 0, 4 do
            for bagSlot = 1, GetContainerNumSlots(bag) do
                local item = GetContainerItemLink(bag, bagSlot)
                if item and (item:find("Troll Sweat") or item:find("Broken Obsidian Club") or item:find("Tarnished Silver Necklace") or item:find("Cracked Pottery") or item:find("Crusted Bandages")) then
                    PickupContainerItem(bag, bagSlot)
                    DeleteCursorItem()
                end
            end
        end
    end
end)
