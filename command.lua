SLASH_ZFTRASH1 = "/zftrash"
SlashCmdList["ZFTRASH"] = function(input, editBox)
    local params = {}

    if input == "" or input == nil then
        print("/zftrash s - Start deleting trash")
        print("/zftrash f - Finish deleting trash")
        return
    end

    local commandList = {}
    local command

    for command in string.gmatch(input, "[^ ]+") do
        table.insert(commandList, command)
    end

    local arg1, arg2 = commandList[1], ""

    for i in pairs(commandList) do
        if i ~= 1 then
            arg2 = arg2 .. commandList[i]
            if commandList[i + 1] ~= nil then
                arg2 = arg2 .. " "
            end
        end
    end

    if arg1 == "s" then
        ZFTrash.active = true
        return
    end

    if arg1 == "f" then
        ZFTrash.active = false
        return
    end
end
